.DEFAULT_GOAL := help

.PHONY: install
install: ## Install the Node dependencies (yarn)
	yarn install

.PHONY: start
start: ## Start a development server
	yarn dev

.PHONY: build
build: ## Build the application
	yarn build

.PHONY: tree
tree:  ## Display the structure of the application
	tree -I 'node_modules|dist' --dirsfirst -CA

.PHONY: help
help:
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
