# Aku

Aku permet de produire des « chroniques d’activité » afin de relever les
actions et les conditions de travail d’une personne observée. Ce logiciel
s’adresse aux ergonomes.

**Si vous souhaitez utiliser Aku, rendez-vous à l’adresse suivante :
[marienfressinaud.frama.io/aku](https://marienfressinaud.frama.io/aku/)**

**Important :** Aku n’est pas parfait, il resterait des choses à faire (voir
[les tickets du dépôt](https://framagit.org/marienfressinaud/aku/-/issues)) et
certaines portions de code ont été écrites un peu rapidement. Cependant, nous
pensons qu’il est déjà complet, considérez-le donc comme terminé. Nous ne
prévoyons pas de l’améliorer dans un futur proche, sauf corrections d’éventuels
bugs.

Le reste de ce document s’adresse aux éventuel·les développeur·ices qui
souhaiteraient contribuer.

## Installation

Aku se présente comme une application Web « _single page_ ». Elle utilise le
framework [Vue.js](https://vuejs.org/) et est packagée avec [Parcel](https://parceljs.org/).
Les dépendances sont gérées avec [yarn](https://yarnpkg.com/).

Première étape : [installez yarn](https://classic.yarnpkg.com/en/docs/install).

Vous pouvez vérifier que yarn est bien installé avec la commande suivante :

```console
$ yarn --version
```

Une fois ceci fait, installez [les dépendances](package.json) :

```console
$ make install
$ # ou
$ yarn install
```

### Environnement de développement

Vous pouvez simplement démarrer un serveur pour servir l’application avec :

```console
$ make start
$ # ou
$ yarn dev
```

Vous devriez pouvoir accéder à l’application après un petit instant à l’adresse
[localhost:1234](http://localhost:1234).

### Environnement de production

Aku a été pensé pour être installé sur un Gitlab Pages, qui consiste à servir
des sites statiques directement depuis un dépôt Gitlab.

Le code de la branche `main` est ainsi [automatiquement construit](.gitlab-ci.yml)
et servi à l’adresse [marienfressinaud.frama.io/aku](https://marienfressinaud.frama.io/aku/).

Si vous souhaitez héberger Aku sur votre propre serveur, vous devrez d’abord
construire l’application :

```console
$ make build
$ # ou
$ yarn build
```

Puis téléversez le répertoire `public/` ainsi généré sur votre serveur en le
renommant en `aku/` et en le plaçant à la racine du répertoire servi par votre
nom de domaine (ex. `/var/www/html/aku/`).

## Crédits

Aku a été conçu par et pour [Maiwann](https://www.maiwann.net/) et développé
par [Marien](https://marienfressinaud.fr/).

## Licence

[Aku](https://framagit.org/marienfressinaud/aku) est placé sous licence [AGPL 3](LICENSE.txt).
