const messages = {
    chronicles: {
        createPage: {
            cancelCreate: 'Revenir aux chroniques',
            cancelImport: 'Annuler',
            existsAlready: 'La chronique que vous essayez de charger existe déjà.',
            fileIsInvalid: 'Le fichier que vous essayez de charger ne correspond pas à un fichier valide.',
            fileMissingKeys: 'Le fichier que vous essayez de charger semble invalide.',
            firstChronicle: 'Bienvenue sur Aku ! Commençons par créer votre première chronique.',
            importLabel: 'Ou importer une chronique',
            importing: 'Vous êtes sur le point de charger la chronique « {chronicle} ».',
            submitCreate: 'Créer',
            submitImport: 'Confirmer',
            titleLabel: 'Comment voulez-vous nommer votre chronique ?',
            titleRequired: 'Ce champ est obligatoire.',
            title: 'Nouvelle chronique',
        },

        dataPage: {
            backToObservation: 'Retourner à l’obervation',
            backToProtocol: 'Créer des observables',
            chartTitle: 'Chronique',
            compress: 'Réduire l’écran',
            dezoom: 'Dézoomer',
            doObservation: 'Effectuer une observation',
            download: 'Télécharger',
            enlarge: 'Élargir l’écran',
            export: 'Exporter',
            exportModal: {
                downloadJson: 'Télécharger au format JSON',
                downloadSvg: 'Télécharger au format SVG',
                intro: 'Votre chronique « {title} » est prête à être exportée dans le format de votre choix.',
                json: 'Au format JSON, votre chronique pourra être envoyée à un·e collègue et être ensuite importée dans l’onglet de création d’une chronique.',
                svg: 'Le format SVG (image) est idéal pour afficher votre chronique dans un rapport et ainsi être visualisée par les personnes n’utilisant pas Aku.',
                title: 'Exporter la chronique',
            },
            fold: 'Replier',
            inProgress: 'Une observation est en cours pour cette chronique.',
            list: {
                endLabel: 'Fin de l’observation',
                groupLabel: 'Groupe',
                intermittentLabel: 'Ponctuel',
                observableLabel: 'Observable',
                startLabel: 'Démarrage de l’observation',
                timeLabel: 'Temps',
            },
            listTitle: 'Relevé',
            noObservables: 'Vous devez d’abord créer des observables et effectuer une observation pour obtenir des données.',
            noObservation: 'Vous devez effectuer une observation pour obtenir des données.',
            stats: {
                duration: 'Durée',
                occurrences: 'Occurences',
                percent: '%',
                time: 'Temps',
                title: 'Statistiques',
            },
            title: 'Données',
            unfold: 'Déplier',
            zoom: 'Zoomer',
        },

        editableTime: {
            mustBeGreater: 'Vous devez saisir une heure supérieure ou égale à {minTime}.',
            mustBeLower: 'Vous devez saisir une heure inférieure ou égale à {maxTime}.',
            submit: 'OK',
        },

        group: {
            nameLabel: 'Nom du groupe',
            newObservable: 'observable',
            observableNameLabel: 'Nom de l’observable',
            observableIntermittentLabel: 'Ponctuel',
            submit: 'OK',
        },

        navigation: {
            data: 'Données',
            observation: 'Observation',
            protocol: 'Protocole',
        },

        observable: {
            intermittentLabel: 'Ponctuel',
            nameLabel: 'Nom de l’observable',
            submit: 'OK',
        },

        observationPage: {
            backToProtocol: 'Créer des observables',
            goToData: 'Voir les données',
            noObservables: 'Vous devez créer des observables pour pouvoir démarrer une observation.',
            restart: 'Reprendre l’observation',
            start: 'Démarrer l’observation',
            stop: 'Arrêter l’observation',
            title: 'Observation',
        },

        page: {
            newChronicle: 'Nouvelle chronique',
            title: 'Chroniques',
        },

        protocolPage: {
            group: 'Groupe',
            groupNameLabel: 'Nom du groupe',
            newGroup: 'Nouveau groupe',
            title: 'Protocole',
        },
    },

    layout: {
        header: {
            goToChronicles: 'Retourner aux chroniques',
        },
    },

    notFoundPage: {
        backHome: 'Clique ici pour les ramener à l’accueil',
        ohNo: 'Oh non ! Ces choupissons se sont enfuis !',
        title: 'Page non trouvée',
    },
};

export default {
    messages,
};
