import Vue from 'vue';
import VueI18n from 'vue-i18n';

import fr from './fr';

Vue.use(VueI18n);

const i18n = new VueI18n({
    locale: 'fr',
    fallbackLocale: 'fr',
    messages: {
        fr: fr.messages,
    },
});

export default i18n;
