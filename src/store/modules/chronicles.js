const state = {
    byIds: {},
};

const getters = {
    list (state) {
        return Object.values(state.byIds);
    },

    findById (state) {
        return (chronicleId) => {
            return state.byIds[chronicleId] || null;
        };
    },
};

const actions = {
    load ({ commit }) {
        const rawChronicles = localStorage.getItem('chronicles');
        const chronicles = rawChronicles ? JSON.parse(rawChronicles) : [];
        commit('addList', chronicles);
    },

    save ({ state }) {
        const chronicles = Object.values(state.byIds);
        localStorage.setItem('chronicles', JSON.stringify(chronicles));
    },
};

const mutations = {
    addList (state, chronicles) {
        let byIds = {};
        chronicles.forEach((chronicle) => {
            byIds[chronicle.id] = chronicle;
        });

        state.byIds = {
            ...state.byIds,
            ...byIds,
        };
    },

    add (state, chronicle) {
        state.byIds = {
            ...state.byIds,
            [chronicle.id]: chronicle,
        };
    },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
