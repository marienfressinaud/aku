const state = {
    byIds: {},
    byGroupIds: {},
};

const getters = {
    list (state) {
        return Object.values(state.byIds);
    },

    listByGroupId (state) {
        return (groupId) => {
            const ids = state.byGroupIds[groupId] || [];

            const observables = [];
            ids.forEach((id) => {
                observables.push(state.byIds[id]);
            });

            return observables;
        };
    },

    countByGroupId (state, getters) {
        return (groupId) => {
            return getters.listByGroupId(groupId).length;
        };
    },

    getById (state) {
        return (id) => {
            return state.byIds[id] || null;
        };
    },
};

const actions = {
    load ({ commit }) {
        const rawObservables = localStorage.getItem('observables');
        const observables = rawObservables ? JSON.parse(rawObservables) : [];
        commit('addList', observables);
    },

    save ({ state }) {
        const observables = Object.values(state.byIds);
        localStorage.setItem('observables', JSON.stringify(observables));
    },
};

const mutations = {
    addList (state, observables) {
        let byIds = {};
        let byGroupIds = {};
        observables.forEach((observable) => {
            byIds[observable.id] = observable;
            if (!byGroupIds[observable.groupId]) {
                byGroupIds[observable.groupId] = [];
            }
            byGroupIds[observable.groupId].push(observable.id);
        });

        state.byIds = {
            ...state.byIds,
            ...byIds,
        };
        state.byGroupIds = {
            ...state.byGroupIds,
            ...byGroupIds,
        };
    },

    add (state, observable) {
        state.byIds = {
            ...state.byIds,
            [observable.id]: observable,
        };

        state.byGroupIds = {
            ...state.byGroupIds,
            [observable.groupId]: [
                ...(state.byGroupIds[observable.groupId] || []),
                observable.id,
            ],
        };
    },

    update (state, payload) {
        state.byIds[payload.id] = {
            ...state.byIds[payload.id],
            ...payload,
        };
    },

    remove (state, id) {
        const byIds = { ...state.byIds };
        const observable = byIds[id];
        const groupObsIds = state.byGroupIds[observable.groupId];

        state.byGroupIds = {
            ...state.byGroupIds,
            [observable.groupId]: groupObsIds.filter((observableId) => {
                return observableId !== id;
            }),
        };

        delete byIds[id];
        state.byIds = byIds;
    },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
