const state = {
    byIds: {},
    byChronicleIds: {},
};

const getters = {
    list (state) {
        return Object.values(state.byIds);
    },

    listByChronicleId (state) {
        return (chronicleId) => {
            const ids = state.byChronicleIds[chronicleId] || [];
            const groups = [];
            ids.forEach((id) => {
                groups.push(state.byIds[id]);
            });

            return groups;
        };
    },

    listNotEmptyByChronicleId (state, getters, rootState, rootGetters) {
        return (chronicleId) => {
            return getters
                .listByChronicleId(chronicleId)
                .filter((group) => {
                    return rootGetters['observables/countByGroupId'](group.id) > 0;
                });
        };
    },

    getById (state) {
        return (id) => {
            return state.byIds[id] || null;
        };
    },
};

const actions = {
    load ({ commit }) {
        const rawGroups = localStorage.getItem('groups');
        const groups = rawGroups ? JSON.parse(rawGroups) : [];
        commit('addList', groups);
    },

    save ({ state }) {
        const groups = Object.values(state.byIds);
        localStorage.setItem('groups', JSON.stringify(groups));
    },
};

const mutations = {
    addList (state, groups) {
        let byIds = {};
        let byChronicleIds = {};
        groups.forEach((group) => {
            byIds[group.id] = group;
            if (!byChronicleIds[group.chronicleId]) {
                byChronicleIds[group.chronicleId] = [];
            }
            byChronicleIds[group.chronicleId].push(group.id);
        });

        state.byIds = {
            ...state.byIds,
            ...byIds,
        };
        state.byChronicleIds = {
            ...state.byChronicleIds,
            ...byChronicleIds,
        };
    },

    add (state, group) {
        state.byIds = {
            ...state.byIds,
            [group.id]: group,
        };

        state.byChronicleIds = {
            ...state.byChronicleIds,
            [group.chronicleId]: [
                ...(state.byChronicleIds[group.chronicleId] || []),
                group.id,
            ],
        };
    },

    setName (state, payload) {
        state.byIds = {
            ...state.byIds,
            [payload.id]: {
                ...state.byIds[payload.id],
                name: payload.name,
            },
        };
    },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
