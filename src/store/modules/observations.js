const state = {
    byIds: {},
    byChronicleIds: {},
};

const getters = {
    getByChronicleId (state) {
        return (chronicleId) => {
            const observationId = state.byChronicleIds[chronicleId];
            if (observationId) {
                const observation = state.byIds[observationId];
                if (!observation.pauses) {
                    observation.pauses = [];
                }
                return observation;
            } else {
                return null;
            }
        };
    },
};

const actions = {
    load ({ commit }) {
        const rawObservations = localStorage.getItem('observations');
        const observations = rawObservations ? JSON.parse(rawObservations) : [];
        observations.forEach((observation) => {
            observation.startAt = new Date(observation.startAt);
            if (observation.endAt) {
                observation.endAt = new Date(observation.endAt);
            }
        });
        commit('addList', observations);
    },

    save ({ state }) {
        const observations = Object.values(state.byIds);
        localStorage.setItem('observations', JSON.stringify(observations));
    },
};

const mutations = {
    addList (state, observations) {
        let byIds = {};
        let byChronicleIds = {};
        observations.forEach((observation) => {
            byIds[observation.id] = observation;
            byChronicleIds[observation.chronicleId] = observation.id;
        });

        state.byIds = {
            ...state.byIds,
            ...byIds,
        };
        state.byChronicleIds = {
            ...state.byChronicleIds,
            ...byChronicleIds,
        };
    },

    add (state, observation) {
        state.byIds = {
            ...state.byIds,
            [observation.id]: observation,
        };

        state.byChronicleIds = {
            ...state.byChronicleIds,
            [observation.chronicleId]: observation.id,
        };
    },

    addData (state, payload) {
        state.byIds[payload.id] = {
            ...state.byIds[payload.id],
            data: [
                ...state.byIds[payload.id].data,
                payload.data,
            ],
        };
    },

    addPause (state, payload) {
        state.byIds[payload.id] = {
            ...state.byIds[payload.id],
            pauses: [
                ...(state.byIds[payload.id].pauses || []),
                payload.pause,
            ],
        };
    },

    setActive (state, payload) {
        state.byIds[payload.id] = {
            ...state.byIds[payload.id],
            activeByGroupIds: {
                ...state.byIds[payload.id].activeByGroupIds,
                [payload.groupId]: payload.observableId,
            },
        };
    },

    update (state, payload) {
        state.byIds[payload.id] = {
            ...state.byIds[payload.id],
            ...payload,
        };
    },

    updateDataAt (state, payload) {
        state.byIds = {
            ...state.byIds,
            [payload.id]: {
                ...state.byIds[payload.id],
                data: state.byIds[payload.id].data.map((d) => {
                    if (
                        d.observableId === payload.data.observableId &&
                        d.at === payload.data.at
                    ) {
                        return {
                            ...d,
                            at: payload.newAt,
                        };
                    } else {
                        return d;
                    }
                }),
            },
        };
    },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
