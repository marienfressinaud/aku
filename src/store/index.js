import Vue from 'vue';
import Vuex from 'vuex';

import chronicles from './modules/chronicles.js';
import groups from './modules/groups.js';
import observables from './modules/observables.js';
import observations from './modules/observations.js';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        chronicles,
        groups,
        observables,
        observations,
    },

    state: {
        appIsReady: false,
    },

    actions: {
        save ({ dispatch }) {
            dispatch('chronicles/save');
            dispatch('groups/save');
            dispatch('observables/save');
            dispatch('observations/save');
        },

        load ({ commit, dispatch }) {
            dispatch('chronicles/load');
            dispatch('groups/load');
            dispatch('observables/load');
            dispatch('observations/load');
            commit('appReady');
        },
    },

    mutations: {
        appReady (state) {
            state.appIsReady = true;
        },
    },

    strict: process.env.NODE_ENV !== 'production',
})
