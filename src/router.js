import Vue from 'vue';
import VueRouter from 'vue-router';

import ChroniclesPage from 'components/pages/ChroniclesPage.vue';
import CreateChroniclePage from 'components/pages/CreateChroniclePage.vue';

import ContainerChroniclePage from 'components/pages/ContainerChroniclePage.vue';
import ProtocolChroniclePage from 'components/pages/ProtocolChroniclePage.vue';
import ObservationChroniclePage from 'components/pages/ObservationChroniclePage.vue';
import DataChroniclePage from 'components/pages/DataChroniclePage.vue';

import NotFoundPage from 'components/pages/NotFoundPage.vue';

import i18n from './locales';

Vue.use(VueRouter);

const routes = [
    { path: '/', redirect: '/chronicles' },

    {
        path: '/chronicles',
        component: ChroniclesPage,
        meta: { title: i18n.t('chronicles.page.title') },
    },

    {
        path: '/chronicles/new',
        component: CreateChroniclePage,
        meta: { title: i18n.t('chronicles.createPage.title') },
    },

    {
        path: '/chronicles/:id',
        component: ContainerChroniclePage,
        children: [
            {
                path: '',
                redirect: (to) => {
                    return `/chronicles/${to.params.id}/protocol`;
                },
            },

            {
                path: 'protocol',
                component: ProtocolChroniclePage,
                meta: { title: i18n.t('chronicles.protocolPage.title') },
            },

            {
                path: 'observation',
                component: ObservationChroniclePage,
                meta: { title: i18n.t('chronicles.observationPage.title') },
            },

            {
                path: 'data',
                component: DataChroniclePage,
                meta: { title: i18n.t('chronicles.dataPage.title') },
            },
        ],
    },

    { path: '*', component: NotFoundPage, meta: { title: i18n.t('notFoundPage.title') } },
];

const router = new VueRouter({
    mode: 'hash',
    base: process.env.NODE_ENV === 'production' ? '/aku/' : '/',
    routes,
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 };
    },
});

router.beforeEach((to, from, next) => {
    const { title } = to.meta;
    document.title = title ? title : 'Aku';

    next();
});

export default router;
