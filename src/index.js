import Vue from 'vue';

import i18n from './locales';
import router from './router.js';
import store from './store';

import TheLayout from 'components/layout/TheLayout.vue';
import TheModal from 'components/layout/TheModal.vue';

Vue.component('the-layout', TheLayout);
Vue.component('the-modal', TheModal);

Vue.directive('autofocus', {
    inserted: function (el) {
        el.focus()
    }
})

new Vue({
    template: '<router-view v-if="appIsReady"></router-view>',
    router,
    store,
    i18n,

    computed: {
        appIsReady () {
            return this.$store.state.appIsReady;
        },
    },

    mounted () {
        this.$store.dispatch('load');
    },
}).$mount('#app');
